export default function (context) {
  // If the user is not authenticated
  if (context.store.getters['user/getLoggedIn'] === false) {
    return context.redirect('/users/login')
  }
}