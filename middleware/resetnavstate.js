export default function (context) {
  if (context.store.getters['menu/getNavState'] === true) {
    context.store.dispatch('menu/switchGlobalNav')
  }
  console.log("From Path ", context.from.path)
  context.store.dispatch('menu/updateCurrentPath', context.from.path)
}