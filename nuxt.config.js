
export default {
  mode: 'spa',
  subdirectory: '/',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Vertical Innovations',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato:300,400,700,900' }
    ],
    script: []
  },
  env: {
    siteUrl: process.env.BASE_URL || 'https://shop.vertical-innovations.com',
    locationAPIBaseUrl: 'https://www.google.com/maps/search/?api=1&query='
  },
  router: {
    base: '/',
    middleware: ['user-agent', 'resetnavstate'],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#ec3850', throttle: 200, height: '3px' },
  loadingIndicator: {
    name: 'folding-cube',
    color: '#ec3850',
    background: 'white'
  },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/scss/app.scss',
    'swiper/dist/css/swiper.css'
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
     { src: '~/plugins/plugin.js', ssr:false },
     { src: '~/plugins/localStorage.js', ssr:false },
     { src: '~/plugins/vue-notifications.js', ssr:false },
     { src: '~/plugins/axios.js', ssr:false },
    //  { src: '~/plugins/vue-fb-customer-chat.js', ssr: false },
     "@/plugins/vue-social-sharing.js",
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    'bootstrap-vue/nuxt',
    'vue-scrollto/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    '@nuxtjs/sentry',
  ],

  buildModules: [
    ['@nuxtjs/google-analytics', {
      id: 'UA-167751097-1'
    }],
    '@nuxtjs/moment',
  ],

  moment: {
    // locales: ['en'],
    defaultLocale: 'en',
    defaultTimezone: 'Asia/Dhaka',
  },

  sentry: {
    dsn: 'https://83763b75394447b7b1c2b3adc5712904@o46969.ingest.sentry.io/5270543',
    config: {
      publishRelease: true,
      attachCommits: true
    },
  },

  proxy: {
    '/api': {
      target: 'https://dash.vertical-innovations.com',
      pathRewrite: {
        '^/api' : '/'
      },
      // changeOrigin: false,
      // prependPath: false
    }
  },

  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    proxy: true
  },
  generate: {
    fallback: true
  },
  /*
  ** Build configuration
  */
  build: {
    transpile: [
      "vee-validate/dist/rules"
    ],
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  }
}
