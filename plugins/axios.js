export default function ({$axios, redirect, store}) {
    $axios.setHeader('Content-Type', 'application/x-www-form-urlencoded', [
      'post', 'put', 'patch'
    ])
    // $axios.setHeader('Access-Control-Allow-Origin', '*')
    $axios.onRequest(config => {
      const token = store.state.user.token
      if(token) {
        config.headers.common['Authorization'] = token
        // console.log(token)
      }
      // console.log(config)
      console.log('Making request to ' + config.url)
    })

    // $axios.onError(error => {
    //   const code = parseInt(error.response && error.response.status)
    //   if (code === 404) {
    //     redirect('/404')
    //   }
    // })
  }