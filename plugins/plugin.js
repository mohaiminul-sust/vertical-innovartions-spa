import Vue from 'vue'
import VueAwesomeSwiper from 'vue-awesome-swiper/dist/ssr'
import Vue2Filters from 'vue2-filters'
import { VueMasonryPlugin } from 'vue-masonry'
import InfiniteLoading from 'vue-infinite-loading'
import VueLazyLoad from 'vue-lazyload'
import VueGeolocation from 'vue-browser-geolocation'
import VueOffline from 'vue-offline'

import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'


Vue.use(VueAwesomeSwiper)
Vue.use(Vue2Filters)
Vue.use(VueMasonryPlugin)
Vue.use(InfiniteLoading)
Vue.use(VueLazyLoad)
Vue.use(VueGeolocation)
Vue.use(VueOffline, {
    mixin: true,
    storage: false
})
Vue.use(Viewer)