import Vue from 'vue'
import VueFbCustomerChat from 'vue-fb-customer-chat'

Vue.use(VueFbCustomerChat, {
  page_id: 'vertical.innovations',
  theme_color: '#ec3850', // theme color in HEX
  locale: 'en_US', // default 'en_US'
})