import Vue from 'vue'
import Vuex from 'vuex'
import menu from './modules/menu'
import layout from './modules/layout'
import user from './modules/user'
import shop from './modules/shop'
import filters from './modules/filters'
import productcart from './modules/productcart'

Vue.use(Vuex)

const createStore = () => {
  return new Vuex.Store({
    modules: {
      menu,
      layout,
      user,
      filters,
      shop,
      productcart,
    }
  })
}
export default createStore
