const state = {
  server_error: null,
  types: [],
  companies: [],
  zones: [],
  areas: [],
  classes: [],
  sizes: [],
  appliedTypes: [],
  appliedCompanies: [],
  appliedAreas: [],
  appliedZones: [],
  appliedClasses: [],
  appliedSizes: [],
  tags: [],
  products: [],
  product: null,
  userLat: null,
  userLon: null,
}

// getters
const getters = {
  getError: state => {
    return state.server_error
  },
  getTypes: state => {
    return state.types
  },
  getCompanies: state => {
    return state.companies
  },
  getAreas: state => {
    return state.areas
  },
  getZones: state => {
    return state.zones
  },
  getClasses: state => {
    return state.classes
  },
  getSizes: state => {
    return state.sizes
  },
  getTags: state => {
    return [...new Set(state.tags)]
  },
  getProducts: state => {
    return state.products
  },
  getProduct: state => {
    return state.product
  },
  getCurrentLat: state => {
    return state.userLat
  },
  getCurrentLon: state => {
    return state.userLon
  }
}

// mutations
const mutations = {
  setError: (state, payload) => {
    state.server_error = payload
  },
  clearError: (state) => {
    if (state.server_error !== null)
      state.server_error = null
  },
  setFilters: (state, payload) => {
    state.zones = payload['ru_zones']
    state.areas = payload['ru_areas']
    state.types = payload['inventory_types']
    state.companies = payload['product_companies']
    state.classes = payload['product_classes']
    state.sizes = payload['product_sizes']
  },
  addAppliedZones: (state, payload) => {
    state.appliedZones = payload
    // state.tags += state.appliedAreas.map(x => x.name)
  },
  addAppliedAreas: (state, payload) => {
    state.appliedAreas = payload
    // state.tags += state.appliedAreas.map(x => x.name)
  },
  addAppliedTypes: (state, payload) => {
    state.appliedTypes = payload
    // state.tags += state.appliedTypes.map(x => x.name)
  },
  addAppliedCompanies: (state, payload) => {
    state.appliedCompanies = payload
    // state.tags += state.appliedCompanies.map(x => x.brand)
  },
  addAppliedClasses: (state, payload) => {
    state.appliedClasses = payload
    // state.tags += state.appliedClasses.map(x => x.name)
  },
  addAppliedSizes: (state, payload) => {
    state.appliedSizes = payload
    // state.tags += state.appliedSizes.map(x => x.name)
  },
  clearAllFilters: (state) => {
    state.appliedZones = []
    state.appliedAreas = []
    state.appliedTypes = []
    state.appliedCompanies = []
    state.appliedClasses = []
    state.appliedSizes = []
  },
  setProducts: (state, payload) => {
    state.products = payload
  },
  setProduct: (state, payload) => {
    state.product = payload
  },
  setUserLocation: (state, payload) => {
    state.userLat = payload.lat.toFixed(5) ?? null
    state.userLon = payload.lng.toFixed(5) ?? null
  }
}

// actions
const actions = {
  async fetchShopFilters(context) {
    try {
      const sfdata = await this.$axios.get('/api/spa/shop-filters/')
      context.commit('setFilters', sfdata['data'])
      context.commit('clearError')
    } catch (error) {
      const err_str = error.response
      console.log(err_str)
      context.commit('setError', err_str)
    }
  },
  appliedAreasFilter(context, payload) {
    context.commit('addAppliedAreas', payload)
  },
  appliedZonesFilter(context, payload) {
    context.commit('addAppliedZones', payload)
  },
  appliedTypesFilter(context, payload) {
    context.commit('addAppliedTypes', payload)
  },
  appliedCompaniesFilter(context, payload) {
    context.commit('addAppliedCompanies', payload)
  },
  appliedClassesFilter(context, payload) {
    context.commit('addAppliedClasses', payload)
  },
  appliedSizesFilter(context, payload) {
    context.commit('addAppliedSizes', payload)
  },
  clearFilters(context) {
    context.commit('clearAllFilters')
  },
  async fetchProducts(context) {
    try {
      const params = {
        lat: context.state.userLat ?? '',
        lon: context.state.userLon ?? '',
        zones: JSON.stringify(context.state.appliedZones.map(x => x.id)),
        areas: JSON.stringify(context.state.appliedAreas.map(x => x.id)),
        types: JSON.stringify(context.state.appliedTypes.map(x => x.id)),
        companies: JSON.stringify(context.state.appliedCompanies.map(x => x.id)),
        classes: JSON.stringify(context.state.appliedClasses.map(x => x.id)),
        sizes: JSON.stringify(context.state.appliedSizes.map(x => x.id)),
      }
      const pdata = await this.$axios.get('/api/spa/remote-units/', {params})
      context.commit('setProducts', pdata['data']['devices'])
      context.commit('clearError')
    } catch (error) {
      this.$sentry.captureException(error)
      const err_str = error.response
      console.log(err_str)
      context.commit('setError', err_str)
    }
  },
  async fetchSingleProductFromTag(context, payload) {
    try {
      const params = {
        tag: payload.trim(),
      }
      const pdata = await this.$axios.get('/api/spa/remote-units/single/', {params})
      console.log(pdata)
      context.commit('setProduct', pdata['data']['device'])
      context.commit('clearError')
      return pdata['data']['device']
    } catch (error) {
      this.$sentry.captureException(error)
      const err_str = error.response
      console.log(err_str)
      context.commit('setError', err_str)
    }
  },
  updateFilterLocation(context, payload) {
    context.commit('setUserLocation', payload)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
