import Menu from '../../data/menu'

const state = {
  data: Menu.data,
  openmobilenav: false,
  currentPath: '/',
  showHeader: true,
  showFooter: true
}

const getters = {
  getNavState: state => {
    return state.openmobilenav
  },
  getCurrentPath: state => {
    return state.currentPath
  },
}

const mutations = {
  switchMobileNav: (state) => {
    state.openmobilenav = !state.openmobilenav
  },
  setCurrentPath: (state, payload) => {
    state.currentPath = payload
  },
  switchHeaderVisible: (state) => {
    state.showHeader = !state.showHeader
  },
  switchFooterVisible: (state) => {
    state.showFooter = !state.showFooter
  }
}

const actions = {
  switchGlobalNav(context) {
    context.commit('switchMobileNav')
  },
  updateCurrentPath(context, payload) {
    context.commit('setCurrentPath', payload)
  },
  switchLayoutMode(context) {
    context.commit('switchHeaderVisible')
    context.commit('switchFooterVisible')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
