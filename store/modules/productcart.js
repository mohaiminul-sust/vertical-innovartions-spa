const state = {
  server_error: null,
  products: [],
  ru: null,
}

// getters
const getters = {
  getError: state => {
    return state.server_error
  },
  getProducts: state => {
    state.products.forEach((element) => {
      element.quantity = isNaN(element.quantity) || element.quantity === null ? 0 : parseInt(element.quantity)
    })
    return state.products
  },
  getTotalAmount: state => {
    if (!state.products.length)
      return 0
    var total = 0
    for(const product of state.products) {
      total += product.unit_price * product.quantity
    }
    return parseFloat(total).toFixed(2)
  },
  getRu: state => {
    return state.ru
  },
}

// mutations
const mutations = {
  setError: (state, payload) => {
    state.server_error = payload
  },
  clearError: (state) => {
    if (state.server_error !== null)
      state.server_error = null
  },
  addProduct: (state, payload) => {
    if (state.ru) {
      if (state.ru.tag === payload.ru.tag) {
        const product = payload
        var cartItem = state.products.find(item => item.id === product.id)
        const qty = product.quantity !== 0 && product.quantity > 1 ? product.quantity : 1
        if (cartItem) {
          if ((cartItem.quantity + qty) <= cartItem.product_count) {
            cartItem.quantity += qty
          } else {
            cartItem.quantity = cartItem.product_count
          }
        } else {
          state.products.push({
            ...product
          })
        }
      } else {
        return
      }
    } else {
      state.products.push({
        ...payload
      })
      state.ru = payload.ru
    }
  },
  removeProduct: (state, payload) => {
    const index = state.products.indexOf(payload)
    state.products.splice(index, 1)
    if (state.products.length === 0)
      state.ru = null
  },
  clearCart: (state) => {
    state.products = []
    state.ru = null
  },
  incrementQuantity: (state, payload) => {
    const product = state.products.find(item => item.id === payload.id)
    if (product.product_count > product.quantity) {
      product.quantity += 1
    }
  },
  decrementQuantity: (state, payload) => {
    const product = state.products.find(item => item.id === payload.id)
    product.quantity -= 1
  }
}

// actions
const actions = {
  addCartItem(context, payload) {
    context.commit('addProduct', payload)
  },
  removeCartItem(context, payload) {
    const product = context.state.products.find(item => item.id === payload.id)
    context.commit('removeProduct', product)
  },
  clearCartItems(context) {
    context.commit('clearCart')
  },
  incrementCartQuantity(context, payload) {
    const product = context.state.products.find(item => item.id === payload.id)
    context.commit('incrementQuantity', product)
  },
  decrementCartQuantity(context, payload) {
    const product = context.state.products.find(item => item.id === payload.id)
    if (product.quantity <= 1) {
      context.commit('removeProduct', product)
    } else {
      context.commit('decrementQuantity', payload)
    }
  },
  async cartCheckout(context) {
    try {
      const payload = {
        cart_items: context.state.products,
        ru_device: context.state.ru
      }
      // console.log(payload)
      const idata = await this.$axios.post('/api/transactions/payment/ssl/', payload)
      // console.log(idata['data'])
      const redirect_url = idata['data']['ssl_gateway_data']['redirect_uri']
      console.log('Redirecting to -> ', redirect_url)
      context.commit('clearError')
      if(redirect_url)
        window.location = redirect_url
        context.commit('clearCart')
    } catch (error) {
      console.log(error.response)
      context.commit('setError', error.response['data']['detail'])
    }
  },
  async cartCheckoutBkash(context) {
    try {
      const payload = {
        cart_items: context.state.products,
        ru_device: context.state.ru
      }
      // console.log(payload)
      const idata = await this.$axios.post('/api/transactions/bkash/web/create/', payload)
      // console.log(idata['data'])
      const redirect_url = idata['data']['bkash_gateway_data']['redirect_uri']
      console.log('Redirecting to -> ', redirect_url)
      context.commit('clearError')
      if(redirect_url)
        window.location = redirect_url
        context.commit('clearCart')
    } catch (error) {
      console.log(error.response)
      context.commit('setError', error.response['data']['detail'])
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
