const state = {
  server_error: null,
  contact: '',
  hotlines: [],
  footer_hotline: '',
  header_hotline: '',
  faqs: [],
  home_ad_banner: null,
  cart_ad_strip: null,
  dispense_ad_strip: null,
  home_banner: null,
  sliders: [],
  services: [],
  partners: [],
  inventory_types: [],
  product_types: [],
  home_video: null,
  top_products: [],
  top_blogs: [],
  avg_rating: 0,
  spa_management_params: null,
}

// getters
const getters = {
  getError: state => {
    return state.server_error
  },
  getContact: state => {
    return state.contact
  },
  getHotlines: state => {
    return state.hotlines
  },
  getHeaderHotline: state => {
    return state.header_hotline
  },
  getFooterHotline: state => {
    return state.footer_hotline
  },
  getFaqs: state => {
    return state.faqs
  },
  getHomeBanner: state => {
    return state.home_banner
  },
  getSliders: state => {
    return state.sliders
  },
  getServices: state => {
    return state.services
  },
  getPartners: state => {
    return state.partners
  },
  getHomeVideo: state => {
    var video_obj = state.home_video
    if (video_obj !== null) {
      video_obj.link = video_obj.link.replace(/watch\?v=/, 'embed/')
    }
    return video_obj
  },
  getInventoryTypes: state => {
    return state.inventory_types
  },
  getProductTypes: state => {
    return state.product_types
  },
  getTopProducts: state => {
    return state.top_products
  },
  getTopBlogs: state => {
    return state.top_blogs
  },
  getHomeAdBanner: state => {
    return state.home_ad_banner
  },
  getCartAdStrip: state => {
    return state.cart_ad_strip
  },
  getDispenseAdStrip: state => {
    return state.dispense_ad_strip
  },
  getSpaManagementParams: state => {
    return state.spa_management_params
  }
}

// mutations
const mutations = {
  setError: (state, payload) => {
    state.server_error = payload
  },
  clearError: (state, payload) => {
    if (state.server_error !== null)
      state.server_error = null
  },
  setContacts: (state, payload) => {
    state.avg_rating = payload['avg_rating']
    state.contact = payload['contact']
    state.hotlines = payload['hotline_contacts']
    state.header_hotline = payload['header_contact']
    state.footer_hotline = payload['footer_contact']
  },
  setFaqs: (state, payload) => {
    state.faqs = payload
  },
  setHomeContent: (state, payload) => {
    // state.home_ad_banner = payload['home_ad_banner']
    state.home_banner = payload['home_banner']
    state.sliders = payload['sliders']
    state.services = payload['services']
    state.partners = payload['partners']
    state.inventory_types = payload['inventory_types']
    state.product_types = payload['product_types']
    state.home_video = payload['home_video']
    state.top_products = payload['top_products']
    state.top_blogs = payload['top_blogs']
  },
  setAdContent: (state, payload) => {
    state.home_ad_banner = payload['home_ad_banner']
    state.cart_ad_strip = payload['cart_ad_strip']
    state.dispense_ad_strip = payload['dispense_ad_strip']
  },
  setSpaManagementParams: (state, payload) => {
    state.spa_management_params = payload
  }
}

// actions
const actions = {
  async fetchSpaManagementParams(context) {
    try {
      const cdata = await this.$axios.get('/api/spa/platform/state/')
      // console.log(cdata['data']);
      context.commit('setSpaManagementParams', cdata['data'])
      context.commit('clearError')
    } catch (error) {
      const err_str = error.response
      console.log(err_str)
      context.commit('setError', err_str)
    }
  },
  async fetchShopContact(context) {
    try {
      const cdata = await this.$axios.get('/api/spa/contact/')
      // console.log(cdata['data']);
      context.commit('setContacts', cdata['data'])
      context.commit('clearError')
    } catch (error) {
      const err_str = error.response
      console.log(err_str)
      context.commit('setError', err_str)
    }
  },
  async fetchShopFaqs(context) {
    try {
      const faqdata = await this.$axios.get('/api/spa/faq/')
      // console.log(faqdata['data']);
      context.commit('setFaqs', faqdata['data']['faqs'])
      context.commit('clearError')
    } catch (error) {
      const err_str = error.response
      console.log(err_str)
      context.commit('setError', err_str)
    }
  },
  async postReview(context, payload) {
    try {
      await this.$axios.post('/api/feedbacks/', payload)
      context.commit('clearError')
    } catch (error) {
      const err_str = error.response
      console.log(err_str)
    }
  },
  async fetchHomeContent(context) {
    try {
      const hdata = await this.$axios.get('/api/spa/home/')
      // console.log(hdata['data']);
      context.commit('setHomeContent', hdata['data'])
      context.commit('clearError')
    } catch (error) {
      const err_str = error.response
      console.log(err_str)
      context.commit('setError', err_str)
    }
  },
  async fetchAdContent(context) {
    try {
      const adata = await this.$axios.get('/api/spa/ads/')
      // console.log(hdata['data']);
      context.commit('setAdContent', adata['data'])
      context.commit('clearError')
    } catch (error) {
      const err_str = error.response
      console.log(err_str)
      context.commit('setError', err_str)
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
