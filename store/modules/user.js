const state = {
    server_error: null,
    isLoggedIn: false,
    token: '',
    // isLoggedIn: localStorage.getItem('loggedin') || false,
    // token: localStorage.getItem('token') ? 'Token '+localStorage.getItem('token') : '',
    user_mail: '',
    personal_info: null,
    billing_info: null,
    rfid_info: {},
    transactions: [],
    recharges: [],
    otp_sid: null,
    liked: false,
    rating: null,
}

const getters = {
    getLoggedIn: state => {
        return state.isLoggedIn
    },
    getError: state => {
        return state.server_error
    },
    getToken: state => {
        return state.token
    },
    getUserMail: state => {
        return state.user_mail
    },
    getPersonalInfo: state => {
        return state.personal_info
    },
    getBillingInfo: state => {
        return state.billing_info
    },
    getShippingPassed: state => {
        const billing_pass = state.billing_info.address !== '' && state.billing_info.city !== '' && state.billing_info.country !== '' && state.billing_info.currency !== '' && state.billing_info.postcode !== '' ? true : false
        const personal_pass = state.personal_info.first_name !== '' && state.personal_info.last_name !== '' ? true : false
        return billing_pass && personal_pass ? true : false
    },
    getRFIDInfo: state => {
        return state.rfid_info
    },
    getTransactions: state => {
        return state.transactions
    },
    getRecharges: state => {
        return state.recharges
    },
    getOtpResponse: state => {
        return state.otp_sid
    },
    getLiked: state => {
        return state.liked
    },
    getRating: state => {
        return state.rating
    }
}

const mutations = {
    setError: (state, payload) => {
        state.server_error = payload
    },
    clearError: (state) => {
        state.server_error = null
    },
    setLoggedIn: (state, payload) => {
        state.isLoggedIn = payload
        // localStorage.setItem('loggedin', payload)
    },
    setToken: (state, key) => {
        if(key !== '') {
            state.token = 'Token '+key
            localStorage.setItem('token', key)
        } else {
            state.token = ''
            localStorage.removeItem('token')
        }
    },
    setUserMail: (state, mail) => {
        state.user_mail = mail
    },
    setPersonalInfo: (state, payload) => {
        state.personal_info = payload
    },
    setBillingInfo: (state, payload) => {
        state.billing_info = payload
    },
    setRFIDInfo: (state, payload) => {
        if (payload === null)
            return
        payload['products'].forEach((element, index) => {
            element.serial_no = index + 1
            if (element.credit_type === "UNPAID") {
                element.credit_type = "CREDIT"
            } else if (element.credit_type === "NORMAL") {
                element.credit_type = "PAID"
            }
        });
        state.rfid_info = payload
    },
    setTransactions: (state, payload) => {
        payload.forEach((element, index) => {
            element.serial_no = index + 1
            element.amount = element.amount > 0 ? "৳ " + parseFloat(element.amount).toFixed(2) : 'N/A'
            element.dispensed = element.dispensed === true ? 'Yes' : 'No'
        });
        state.transactions = payload
    },
    setRecharges: (state, payload) => {
        payload.forEach((element, index) => {
            element.serial_no = index + 1
            element.unit_price = "৳ " + parseFloat(element.unit_price).toFixed(2)
            element.amount = "৳ " + parseFloat(element.amount).toFixed(2)
        });
        state.recharges = payload
    },
    setOtpSid: (state, payload) => {
        state.otp_sid = payload
    },
    setLiked: (state, payload) => {
        state.liked = payload
    },
    setRating: (state, payload) => {
        const rating = parseFloat(payload)
        state.rating = rating > 0 ? rating : null
    },
    unsetUserData: (state) => {
        state.liked = false
        state.rating = null
        state.isLoggedIn = false
        state.token = ''
        state.personal_info = null
        state.billing_info = null
        state.rfid_info = null
        state.transactions = []
        state.recharges = []
    }
}

const actions = {
    async loginUserWithEmail(context, payload) {
        console.log(payload)
        try {
            const rdata = await this.$axios.post('/api/rest-auth/login/', payload)
            // console.log(rdata['data']['key']);
            context.commit('unsetUserData')
            context.commit('setLoggedIn', true)
            context.commit('setUserMail', payload.email)
            context.commit('setToken', rdata['data']['key'])
            context.commit('clearError')

            // piggyback user details
            context.dispatch('piggybackLoggedUserdata')
        } catch (error) {
            const err_str = error.response['data']['non_field_errors'][0]
            context.commit('setError', err_str)
        }
    },
    async registerUserWithEmail(context, payload) {
        console.log(payload)
        try {
            const rdata = await this.$axios.post('/api/rest-auth/registration/', payload)
            // console.log(rdata['data']['key']);
            context.commit('unsetUserData')
            context.commit('setLoggedIn', true)
            context.commit('setUserMail', payload.email)
            context.commit('setToken', rdata['data']['key'])
            context.commit('clearError')

            // piggyback user details
            context.dispatch('piggybackLoggedUserdata')
        } catch (error) {
            console.log(error.response['data'])
            const err_str = error.response['data']['email'] ? error.response['data']['email'][0] : error.response['data']['password1'][0]
            console.log(err_str)
            context.commit('setError', err_str)
        }
    },
    async verifyUserWithPhone(context, payload) {
        console.log(payload)
        try {
            const rdata = await this.$axios.post('/api/rest-auth/guest/otp/', payload)
            context.commit('setOtpSid', rdata['data']['otp_sid'])
            context.commit('clearError')
        } catch (error) {
            console.log(error.response['data'])
            const err_str = error.response['data']['detail']
            console.log(err_str)
            context.commit('setError', err_str)
        }
    },
    async loginRegisterUserWithPhone(context, payload, guest=false) {
        console.log(payload)
        try {
            const rdata = await this.$axios.post('/api/rest-auth/guest/otp/verify/', payload)
            context.commit('unsetUserData')
            context.commit('setLoggedIn', true)
            context.commit('setToken', rdata['data']['otp_status']['key'])
            context.commit('clearError')
            context.dispatch('piggybackLoggedUserdata')
        } catch (error) {
            console.log(error.response['data'])
            const err_str = error.response['data']['detail']
            console.log(err_str)
            context.commit('setError', err_str)
        }
    },
    async piggybackLoggedUserdata(context) {
        await context.dispatch('fetchPersonalInfo')
        await context.dispatch('fetchBillingInfo')
        await context.dispatch('fetchRFIDInfo')
        await context.dispatch('fetchLikeRating')
        await context.dispatch('fetchTransactions')
        await context.dispatch('fetchRecharges')
    },
    async updateUserPassword(context, payload) {
        console.log(payload)
        try {
            const rdata = await this.$axios.post('/api/rest-auth/password/change/', payload)
            context.commit('clearError')
        } catch (error) {
            const err_str = error.response['data']['new_password2'][0]
            console.log(err_str)
            context.commit('setError', err_str)
        }
    },
    async forgotUserPassword(context, payload) {
        console.log(payload)
        try {
            const rdata = await this.$axios.post('/api/rest-auth/password/forget/', payload)
            // context.commit('clearError')
            localStorage.clear()
        } catch (error) {
            const err_str = error.response['data']['detail']
            console.log(err_str)
            context.commit('setError', err_str)
        }
    },
    async fetchPersonalInfo(context) {
        try {
            const p_info = await this.$axios.get('/api/personal-info/')
            // console.log(p_info['data'])
            context.commit('clearError')
            context.commit('setPersonalInfo', p_info['data'])
        } catch (error) {
            context.commit('setError', error.response['data']['detail'])
        }
    },
    async fetchBillingInfo(context) {
        try {
            const b_info = await this.$axios.get('/api/billing-info/')
            context.commit('clearError')
            context.commit('setBillingInfo', b_info['data'])
        } catch (error) {
            context.commit('setError', error.response['data']['detail'])
        }
    },
    async fetchRFIDInfo(context) {
        try {
            const r_info = await this.$axios.get('/api/rfid-info/')
            const payload = r_info['data']
            context.commit('setRFIDInfo', payload)
            context.commit('clearError')
        } catch (error) {
            context.commit('setRFIDInfo', null)
            // console.log("RFID ERROR CATCH BLK", error.response['data']['detail'])
            context.commit('setError', error.response['data']['detail'])
        }
    },
    async updatePersonalInfo(context, payload) {
        try {
            const up_info = await this.$axios.put('/api/personal-info/', payload)
            // console.log(up_info['data'])
            context.commit('clearError')
            context.commit('setPersonalInfo', up_info['data'])
        } catch (error) {
            context.commit('setError', error.response['data']['detail'])
        }
    },
    async updateBillingInfo(context, payload) {
        try {
            const ub_info = await this.$axios.put('/api/billing-info/', payload)
            context.commit('clearError')
            context.commit('setBillingInfo', ub_info['data'])
        } catch (error) {
            context.commit('setError', error.response['data']['detail'])
        }
    },
    async fetchTransactions(context) {
        try {
            const tran_data = await this.$axios.get('/api/transactions/')
            // console.log(tran_data['data']['results'])
            const payload = tran_data['data']['results']
            context.commit('setTransactions', payload)
            context.commit('clearError')
        } catch (error) {
            context.commit('setError', error.response['data']['detail'])
        }
    },
    async fetchRecharges(context) {
        try {
            const rec_data = await this.$axios.get('/api/recharge/transactions/')
            // console.log(tran_data['data']['results'])
            const payload = rec_data['data']['results']
            context.commit('setRecharges', payload)
            context.commit('clearError')
        } catch (error) {
            context.commit('setError', error.response['data']['detail'])
        }
    },
    async rechargeRFID(context, payload) {
        try {
            const rec_resp = await this.$axios.post('/api/recharge/payment/', payload)
            const redirect_url = rec_resp['data']['ssl_gateway_data']['redirect_uri']
            console.log(redirect_url)
            if(redirect_url) {
                window.location = redirect_url
                context.dispatch('fetchRecharges')
            }
            context.commit('clearError')
        } catch (error) {
            context.commit('setError', error.response['data']['detail'])
        }
    },
    async fetchLikeRating(context) {
        if (context.state.isLoggedIn) {
            try {
                const lr_data = await this.$axios.get('/api/like-rating/')
                console.log(lr_data['data'])
                context.commit('setLiked', lr_data['data']['liked'])
                context.commit('setRating', lr_data['data']['rating'])
                context.commit('clearError')
            } catch (error) {
                context.commit('setError', error.response)
            }
        }
    },
    async updateLiked(context, payload) {
        try {
            const lr_data = await this.$axios.put('/api/like-rating/', payload)
            console.log(lr_data['data'])
            context.commit('setLiked', lr_data['data']['liked'])
            context.commit('clearError')
        } catch (error) {
            context.commit('setError', error.response)
        }
    },
    async updateRating(context, payload) {
        try {
            const lr_data = await this.$axios.put('/api/like-rating/', payload)
            console.log(lr_data['data'])
            context.commit('setRating', lr_data['data']['rating'])
            context.commit('clearError')
        } catch (error) {
            context.commit('setError', error.response)
        }
    },
    logoutUser(context) {
        context.commit('unsetUserData')
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}